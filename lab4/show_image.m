function show_image(image)

r = image; 
im = reshape(r, 112, 92);
imshow(im, [min(r), max(r)]);