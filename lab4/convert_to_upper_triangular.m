function [A, b] = convert_to_upper_triangular(A, b)

N = length(A);

for i = 1: N
    for j = i+1:N
        c = -A(j,i)/A(i,i);
        A(j, :) = A(j,:) + c * A(i, :);
        b(j, :) = b(j, :) + c * b(i, :);
    end
end