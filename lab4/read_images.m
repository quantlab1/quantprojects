function [images, labels] = read_images()

IMAGE_PATH = 'orl_faces';

labels = zeros(400, 1);
images = zeros(10304, 400);

for i = 1:40
    im_dir = strjoin({IMAGE_PATH, strcat('s', num2str(i)) }, '/');
    labels((i-1)*10+1:i*10) = i;
    for j = 1:10
        im_path = strjoin({im_dir, strcat(num2str(j), '.pgm') }, '/');
        im = imread(im_path);
        images(:, (i-1)*10+j) = im(:);
    end
end
