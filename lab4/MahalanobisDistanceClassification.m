function test_error = MahalanobisDistanceClassification(...
    train_data, train_label, test_data, test_label, K, eigvector, ...
    eigvalue, mean_image)

train_data = train_data - repmat(mean_image, 1, size(train_data, 2));
test_data = test_data - repmat(mean_image, 1, size(test_data, 2));

train_data = eigvector' * train_data;
test_data = eigvector' * test_data;

X = train_data';
Y = train_label;

u_i = cell(K, 1);

for i = 1:K
    Xi = X(Y == i, :);
    u_i{i} = mean(Xi);
end

% Test Accuracy of Train Data
T = size(train_data, 2);
R = zeros(T, K);
for j = 1:R
    for i = 1:K
        R(j, i) = (mahalanobis_distance(u_i{i}', train_data(:, j), eigvalue));
    end
end

% Test Accuracy of Test Data.
L = size(test_data, 2);

D = zeros(L, K);

for j = 1:L
    for i = 1:K
        D(j, i) = (mahalanobis_distance(u_i{i}', test_data(:, j), eigvalue));
    end
end

[test_error, ~] = calculate_error_metric(D, test_label, @min);