# README #

Dimensionality Reduction & Classification


### How do I run the code ? ###

* cd /path/to/project/directory
* run main.m

In main.m, the dimensionality reduction is achieved using PCA with 90%
variance preservation. Following variables contain the test error rate for
various experiment conducted.

* `FNF_Test_Error`  --> 2 Class Face/Non-face classification error rate using lineare regression
* `UK_test_error`   --> 2 Class Known/Unknown classification error rate using linear regresssion
* `F_test_error`    --> 35 Class Known face classification error rate using linear regression
* `FM_test_error`   --> 35 Class Known face classification error rate using Mahalanobis Distance
* `FL_test_error`   --> 35 Class Known face classification error rate using LDA