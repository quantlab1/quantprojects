function test_error = LDAClassification(train_data, train_label, ...
    test_data, test_label, K, eigvector, mean_image)

% K : No of Classes

train_data = train_data - repmat(mean_image, 1, size(train_data, 2));
test_data = test_data - repmat(mean_image, 1, size(test_data, 2));

train_data = eigvector' * train_data;
test_data = eigvector' * test_data;

X = train_data';
Y = train_label;
X_t = test_data';

N = size(train_data, 2);

M = cell(K, 1);
u_i = cell(K, 1);
u = mean(X); % global mean
C = cell(K, 1); % covariance matrix by group
P = zeros(K, 1); % Prior Probability by group

for i = 1:K
    M{i} = X(Y == i, :);
    u_i{i} = mean(M{i});
    M{i} = M{i} - u;
    C{i} = M{i}'* M{i}/size(M{i}, 1);
    P(i) = size(M{i}, 1)/ N;
end

Co = zeros(size(C{1})); % Covariance matrix

for i = 1:K
    Co = Co + P(i)*C{i};
end

Co_Inv = inv(Co);

R = zeros(size(X_t, 1), K);
Y_t = zeros(size(X_t, 1), 1);

for i = 1:size(X_t, 1)
    for j = 1:K
        R(i, j) = (u_i{j}* Co_Inv) * X_t(i, :)' - 1/2*u_i{j}*Co_Inv*u_i{j}' + log(P(j));
    end
end

[test_error, ~] = calculate_error_metric(R, test_label, @max);
