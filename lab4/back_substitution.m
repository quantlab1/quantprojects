function x = back_substitution(A, b)

[m, n] = size(b);
x = zeros(m, n);
[l, c] = size(A);

for i = c:-1:1
    su = zeros(1, n);
    for j = i+1:l
        su = su +  (x(j, :) * A(i, j));
    end
    x(i, :) = ((b(i, :) - su)/A(i, i));
end