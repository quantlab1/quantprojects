function [err_p, err_idx] = calculate_error_metric(IRM, Y_Real, func)
% IRM = Idicator Response Matrix
% Y_Real = Real values with each value having a class as the index.

err_p = 0;
err_idx = [];

L = size(IRM, 1);
for i = 1: size(IRM, 1)
    [~, idx] = func(IRM(i, :));
    if idx ~= Y_Real(i)
        err_p = err_p + 1;
        err_idx = [err_idx i];
    end
end

err_p = err_p / L;