function w = gaussian_elimination(x, y)

[a,b] = convert_to_upper_triangular(x, y);
w = back_substitution(a, b);