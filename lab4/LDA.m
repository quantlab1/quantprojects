[images, labels] = read_images();

train_indices = [];
test_indices = [];

for i = 1:35
    train_indices = [train_indices ((i-1)*10 + 1):(i*10-2)];
    test_indices = [test_indices (i*10-1): i*10];
end

% for i = 36:40
%     test_indices = [test_indices (i-1)*10+1 : i*10];
% end

train_data = images(:, train_indices);
train_label = labels(train_indices);
test_data = images(:, test_indices);
test_label = labels(test_indices);

[eigvector, eigvalue, mean_image] = PCA(train_data, 0.90);

train_data = train_data - mean_image;
test_data = test_data - mean_image;

train_data = eigvector' * train_data;
test_data = eigvector' * test_data;

X = train_data';
Y = train_label;
X_t = test_data';

N = size(train_data, 2);
K = 35 ; % no of classes

M = cell(K, 1);
u_i = cell(K, 1);
u = mean(X); % global mean
C = cell(K, 1); % covariance matrix by group
P = zeros(K, 1); % Prior Probability by group

for i = 1:K
    M{i} = X(Y == i, :);
    u_i{i} = mean(M{i});
    M{i} = M{i} - u;
    C{i} = M{i}'* M{i}/size(M{i}, 1);
    P(i) = size(M{i}, 1)/ N;
end

Co = zeros(size(C{1})); % Covariance matrix

for i = 1:K
    Co = Co + P(i)*C{i};
end

Co_Inv = inv(Co);

R = zeros(size(X_t, 1), K);
Y_t = zeros(size(X_t, 1), 1);

for i = 1:size(X_t, 1)
    for j = 1:K
        R(i, j) = (u_i{j}* Co_Inv) * X_t(i, :)' - 1/2*u_i{j}*Co_Inv*u_i{j}' + log(P(j));
    end
    [~, idx] = max(R(i, :));
    Y_t(i) = idx;
end

[Y_t test_label]