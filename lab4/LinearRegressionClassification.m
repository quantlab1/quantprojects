function [train_error, test_error] = LinearRegressionClassification(...
               train_data, train_label, test_data, test_label, K, P, eigvector, mean_image)

% K : No of classes
% P : Polynomial basis to expand.
L = size(train_label, 1);

IRM = zeros(L, K);

for i = 1:K
    IRM(train_label == i, i) = 1;
end

train_data = train_data - repmat(mean_image, 1, size(train_data, 2));
test_data = test_data - repmat(mean_image, 1, size(test_data, 2));

train_data = eigvector' * train_data;
test_data = eigvector' * test_data;

X = train_data';
X_test = test_data';

Z = ones(size(X, 1), 1);
Z_t = ones(size(X_test, 1), 1);

for i = 1: P
    Z = [Z X.^i];
    Z_t = [Z_t X_test.^i];
end

Z = Z';
Z_t = Z_t';

M = Z * Z';
S = Z * IRM;

W = gaussian_elimination(M, S); % Face/Non-Face Training Matrix

% Prediction on Trained data.
Y = (W' * Z)';

% Prediction on Test data.
Y_test= (W' * Z_t)';

[train_error, ~] = calculate_error_metric(Y, train_label, @max);
[test_error, ~] = calculate_error_metric(Y_test, test_label, @max);