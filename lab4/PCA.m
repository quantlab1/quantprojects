function [eigvector, eigvalue, x_mean] = PCA(X, threshold)

[l, ~] = size(X);
x_mean = zeros(l, 1);
for i = 1: l
    m = mean(X(i, :));
    x_mean(i) = m;
    X(i, :) = X( i, :) - m;
end

z = 0;

Sigma = X/sqrt(l-1);
[U, S, ~] = svd(Sigma);
B = diag(S);
B = B.^2;
s = sum(B);

for k = 1: length(B)
    z = z + B(k);
    if (z / s) > threshold
        break
    end
end

eigvector = U(:, 1:k);
eigvalue = B(1:k);
