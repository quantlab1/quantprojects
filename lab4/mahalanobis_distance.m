function distance = mahalanobis_distance(vec1, vec2, eigvalue)

diff = (vec1 - vec2).^2;
div = diff./eigvalue;
distance = sqrt(sum(div));