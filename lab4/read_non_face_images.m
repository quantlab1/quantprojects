function images = read_non_face_images()

IMAGE_PATH = 'non_faces';
IMAGE_RE = 'non_faces/*.jpg';
files = dir(IMAGE_RE);

K = length(files);
images = zeros(10304, K);
for i = 1:length(files)
    file_name = files(i).name;
    im_path = strjoin({IMAGE_PATH, file_name}, '/');
    im = imread(im_path);
    images(:, i) = im(:);
end