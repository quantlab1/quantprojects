% Face Images and Labels of Individual person.
[face_images, labels] = read_images();

% Non-face image
non_face_images = read_non_face_images();

L = size(face_images(:, 1), 1); % Size of an image in 1D.

% Train and Test indices for Face images.
face_train_indices = [];
face_test_indices = [];

for i = 1:35
    face_train_indices = [face_train_indices ((i-1)*10 + 1):(i*10-2)];
    face_test_indices = [face_test_indices (i*10-1): i*10];
end

face_train_data = face_images(:, face_train_indices);
face_train_label = labels(face_train_indices);
face_test_data = face_images(:, face_test_indices);
face_test_label = labels(face_test_indices);

[eigvector, eigvalue, mean_image] = PCA(face_train_data, 0.90);

%% Face/Non-Face Classification Using Linear Regression.
% This problem is formulated as 2-class classification problem with
% 1 as Face class and 2 as Non-face class

FNF_train_data = zeros(L, 330); % 280 face images + 50 non-face images as training data.
FNF_train_label = zeros(330, 1);
FNF_test_data = zeros(L, 96); % 70 test images + 25 non-face images as test data;
FNF_test_label = zeros(96, 1); % Output Label for the test data.

FNF_train_data(:, 1:280) = face_train_data;
FNF_test_data(:, 1:70) = face_test_data(:, 1:70);
FNF_train_label(1:280) = 1;
FNF_test_label(1:70) = 1;

FNF_train_data(:, 281:330) = non_face_images(:, 1:50);
FNF_test_data(:, 71:96) = non_face_images(:, 51:76);
FNF_train_label(281:330) = 2;
FNF_test_label(71:96) = 2;

fprintf('Using Linear Regression based 2 class Face/Non-Face classification \n');
[FNF_Train_Error, FNF_Test_Error] = LinearRegressionClassification(...
    FNF_train_data, FNF_train_label, FNF_test_data, FNF_test_label, ...
    2, 1, eigvector, mean_image);

fprintf("The Train error rate for 2-Class face/non-face classification is %.2f\n", FNF_Train_Error);
fprintf("The Test error rate for 2-Class face/non-face classification is %.2f\n", FNF_Test_Error);
fprintf("\n\n");


%% Known/Unknown Face Classification Using Linear Regression
% This problem is formulated as 2-class classification problem with one 
% class as known and other class as unknown.

% Train and Test indices for Face images.
UK_train_indices = [];
UK_test_indices = [];

for i = 1:40
    UK_train_indices = [UK_train_indices ((i-1)*10 + 1):(i*10-2)];
    UK_test_indices = [UK_test_indices (i*10-1): i*10];
end

UK_train_data = face_images(:, UK_train_indices);
UK_L = size(UK_train_data, 2);
UK_train_label = zeros(UK_L, 1);
UK_train_label(1:280) = 1;
UK_train_label(281:320) = 2;
UK_test_data = face_images(:, UK_test_indices);

UK_test_label = zeros(size(UK_test_data, 2), 1);
UK_test_label(1:70) = 1;
UK_test_label(71:80) = 2;

UK_K = 2; % 2 class classification
UK_P = 1; % Ploynomial basis

fprintf('Using Linear Regression based 2 Class Known/Unknown classification\n');
[UK_train_error, UK_test_error] = LinearRegressionClassification(...
    UK_train_data, UK_train_label, UK_test_data, UK_test_label, ...
    UK_K, UK_P, eigvector, mean_image);

fprintf("The Train error rate for 35-Class face classification is %.4f\n", UK_train_error);
fprintf("The Test error rate for 35-Class face classification is %.4f\n", UK_test_error);
fprintf("\n\n")

%% Known Face Classification Using Linear Regression
% This problem is formulated as 35-class classification problem.

F_train_data = face_train_data - mean_image;
F_test_data = face_test_data - mean_image;
F_train_label = face_train_label;
F_test_label = face_test_label;

F_K = 35; % 35 class
F_P = 2; % After testing several values of P, P = 2 was found the best.

fprintf('Using Linear Regression based 35 Class Known classification\n');
[F_train_error, F_test_error] = LinearRegressionClassification(...
    F_train_data, F_train_label, F_test_data, F_test_label, ...
    F_K, 2, eigvector, mean_image);

fprintf("The Train error rate for 35-Class face classification is %.4f\n", F_train_error);
fprintf("The Test error rate for 35-Class face classification is %.4f\n", F_test_error);
fprintf("\n\n");


%% Known Face Classification Using Mahalanobis Distance

fprintf('Using Mahalanobis Distance based Known 35 Class classification\n');
FM_test_error = MahalanobisDistanceClassification(...
    F_train_data, F_train_label, F_test_data, F_test_label, F_K,...
    eigvector, eigvalue, mean_image);

fprintf("The Test error rate for 35-Class face classification is %.4f\n", FM_test_error);
fprintf("\n\n");


%% Known Face Classification Using LDA

fprintf('Using LDA based Classification for 35-Class Known face classification\n')

FL_test_error = LDAClassification(F_train_data, F_train_label, ...
    F_test_data, F_test_label, F_K, eigvector, mean_image);

fprintf("The Test error rate for 35-Class face classification is %.4f\n", FL_test_error);
fprintf("\n\n");

