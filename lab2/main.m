threshold = 1e-6;
rho = 0.5;
al = 0.1;
step = 1;


%% Solution for Function 1
X1 = ones(100, 1);
[X1_GD, val1_GD, st1_GD] =  steepest_descent(@func1, @grad_func1, X1, step, rho, al, threshold, @backtrack);
disp(X1_GD);
disp(val1_GD);

[X1_CGD, val1_CGD, st1_CGD] = conjugate_gradient_descent(@func1, @grad_func1, X1, step, rho, al, threshold, @backtrack);
disp(X1_CGD);
disp(val1_CGD);

[X1_N, val1_N, st1_N] = newton_method(@func1, @grad_func1, @hessian_func1, X1, step, rho, al, threshold, @backtrack);
disp(X1_N);
disp(val1_N);

[X1_QN, val1_QN, st1_QN] = quasi_newton(@func1, @grad_func1, X1, step, rho, al, threshold, @backtrack);
disp(X1_QN);
disp(val1_QN);

fprintf("The No of Iterations for Convergence are:\n 1. Gradient Descent %d\n 2. Conjugate Gradient Descent %d\n 3. Newton's Method  %d\n 4. Quasi-Newton Method %d\n", length(st1_GD)-1, length(st1_CGD)-1, length(st1_N)-1, length(st1_QN)-1);
draw_figure(st1_GD, st1_CGD, st1_N, st1_QN, 'Function 1');

%% Solution for Function 2

fA = fopen('fun2_A.txt','r');
fb = fopen('fun2_b.txt','r');
fc = fopen('fun2_c.txt','r');

A = fscanf(fA,'%e',[500,100]);
b = fscanf(fb,'%e',[500,1]);
c = fscanf(fc,'%e',[100,1]);

fclose(fb);
fclose(fA);
fclose(fc);

func2_ = @(x) func2(x, A, b ,c);
grad_func2_ = @(x) grad_func2(x, A, b, c);
hessian_func2_ = @(x) hessian_func2(x, A, b);
backtrack2 = @(f, g, a, p, x, r, al) backtrack_modified(f, g, a, p, x, r, al, A, b);

X2 = zeros(100, 1);
[X2_GD, val2_GD, st2_GD] = steepest_descent(func2_, grad_func2_, X2, step, rho, al, threshold, backtrack2);
disp(X2_GD);
disp(val2_GD);

[X2_CGD, val2_CGD, st2_CGD] = conjugate_gradient_descent(func2_, grad_func2_, X2, step, rho, al, threshold, backtrack2);
disp(X2_CGD);
disp(val2_CGD);

[X2_N, val2_N, st2_N] = newton_method(func2_, grad_func2_, hessian_func2_, X2, step, rho, al, threshold, backtrack2);
disp(X2_N);
disp(val2_N);

[X2_QN, val2_QN, st2_QN] = quasi_newton(func2_, grad_func2_, X2, step, rho, al, threshold, backtrack2);
disp(X2_QN);
disp(val2_QN);

fprintf("The No of Iterations for Convergence are:\n 1. Gradient Descent %d\n 2. Conjugate Gradient Descent %d\n 3. Newton's Method  %d\n 4. Quasi-Newton Method %d\n", length(st2_GD)-1, length(st2_CGD)-1, length(st2_N)-1, length(st2_QN)-1);
draw_figure(st2_GD, st2_CGD, st2_N, st2_QN, 'Function 2');


%% Solution for Function 3
X3 = [-1.2 1]';
[X3_GD, val3_GD, st3_GD] =  steepest_descent(@func3, @grad_func3, X3, step, rho, al, threshold, @backtrack);
disp(X3_GD);
disp(val3_GD);

[X3_CGD, val3_CGD, st3_CGD] = conjugate_gradient_descent(@func3, @grad_func3, X3, step, rho, al, threshold, @backtrack);
disp(X3_CGD);
disp(val3_CGD);

[X3_N, val3_N, st3_N] = newton_method(@func3, @grad_func3, @hessian_func3, X3, step, rho, al, threshold, @backtrack);
disp(X3_N);
disp(val3_N);

[X3_QN, val3_QN, st3_QN] = quasi_newton(@func3, @grad_func3, X3, step, rho, al, threshold, @backtrack);
disp(X3_QN);
disp(val3_QN);

fprintf("The No of Iterations for Convergence are:\n 1. Gradient Descent %d\n 2. Conjugate Gradient Descent %d\n 3. Newton's Method  %d\n 4. Quasi-Newton Method %d\n", length(st3_GD)-1, length(st3_CGD)-1, length(st3_N)-1, length(st3_QN)-1);

draw_figure(st3_GD, st3_CGD, st3_N, st3_QN, 'Function 3');



