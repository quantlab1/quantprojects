function [X, f, st] = quasi_newton(func, grad_func, X, alpha, rho, c, threshold, backtrack_func)
% func - function to optimize
% X - Initial value of Decision variable
% alpha - Starting value of alpha
% rho - provided by question
% c - some constant provided by question
% threshold - some threshold to stop the loop
% BFGS Implementations

M = 500; % Maximum number of iterations allowed

f = func(X);
g = grad_func(X);
V = eye(length(X));

st = [f];

while (norm(g) > threshold) && ( M > 1)
    g_k = g;
    p = -V*g;
    
    a = backtrack_func(func, g, alpha, p, X, rho, c);
    
    s = a*p;
    X = X + s;
    
    g = grad_func(X);
    y = g - g_k;
    
    l = (V* (y * y') * V)/(y'*V*y);
    b = (s * s')/(s' * y);
    
    V = V - l + b;
    
    M = M - 1;
    f = func(X);
    st = [st f];
end