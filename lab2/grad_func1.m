function d = grad_func1(X)
% d = gradient of the function at X

N = length(X);
d = zeros(N, 1);

for i = 1:N
    d(i) = 2 * i * X(i);
end

