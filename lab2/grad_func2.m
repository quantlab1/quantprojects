function d = grad_func2(X, A, b, c)
% y = value of the function at X
% d = gradient of the function at X
% H = Hessian of the function at X

t = (b - A*X);
d = c + A'*(1./t);
