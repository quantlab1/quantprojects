function H = hessian_func2(X, A, b)
% H = Hessian of the function at X


t = (b - A*X);
H = A' * diag(1./(t.^2))*A;
