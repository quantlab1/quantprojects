function f_ = gradient(func, x, order, h)
% x is a column vector

[L, B] = size(x);

f_ = zeros(L, B);
e = eye(L);
e_ = e*h;

for i = 1: L
    e_i = e_(:, i);
    if order == 1
        f_(i, :) = (func(x + e_i) - func(x))/h;
    elseif order == 2
        f_(i, :) = (func(x + e_i) - func(x - e_i))/(2*h);
    elseif order == 4
        f_(i, :) = (-func(x + 2 * e_i) + 8 * func(x + e_i) - 8 * func(x - e_i) + func(x - 2 * e_i))/(12 * h);
    end
end
