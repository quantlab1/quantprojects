function H = hessian_func1(X)
% H = value of the function at X

N = length(X);
H = zeros(N, N);

for i = 1:N
    H(i, :) = [zeros(1, i-1) 2*i zeros(1, N-i)];
end