function y = func2(X, A, b, c)
% y = value of the function at X
% d = gradient of the function at X
% H = Hessian of the function at X
t = (b - A*X);
y = c'*X - sum(log(t));