function [X, f, st] = newton_method(func, grad_func, hess_func, X, alpha, rho, c, threshold, backtrack_func)
% func - function to optimize
% X - Initial value of Decision variable
% alpha - Starting value of alpha
% rho - provided by question
% c - some constant provided by question
% threshold - some threshold to stop the loop

M = 500; % Maximum number of iterations allowed

g = grad_func(X);
st = func(X);

while (norm(g) > threshold) && ( M > 1)
    H = hess_func(X);
    p = -H\g;
    
    a = backtrack_func(func, g, alpha, p, X, rho, c);
    
    X = X + a * p;
    f= func(X);
    g = grad_func(X);
    M = M - 1;
    st = [st f];
end