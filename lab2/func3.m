function y = func3(X)
% y = value of the function at X
% d = gradient of the function at X
% H = Hessian of the function at X
% Rosenbrock function
t = X(2) - X(1)*X(1);
m = (1 - X(1));

y = 100 * t * t + m * m;