function y = func1(X)
% y = value of the function at X

N = length(X);
y = 0;

for i = 1:N
    y = y + i*X(i)*X(i);
end