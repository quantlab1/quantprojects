function [X, f, st] = conjugate_gradient_descent(func, grad_func, X, alpha, rho, c, threshold, backtrack_func)
% func - function to optimize
% grad_func - gradient of function func
% X - Initial value of Decision variable
% alpha - Starting value of alpha
% rho - provided by question
% c - some constant provided by question
% threshold - some threshold to stop the loop

M = 50000;

f = func(X);
g = grad_func(X);
p = -g;

st = f;
N = 50;

while (norm(g) > threshold) && ( M > 1)
    p_k = p;
    g_k = g;
    
    a = backtrack_func(func, g, alpha, p, X, rho, c);
    
    X = X + a * p;
    f = func(X);
    g = grad_func(X);
    
    b = g' * g / (g_k' * g_k);
    if N == 0
        p = -g;
        N=50;
    else
        p = -g + b * p_k;
    end
    N = N -1;
    
    M = M - 1;
    st = [st f];
end