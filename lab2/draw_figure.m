function draw_figure(x1, x2, x3, x4, str)

figure;
x1 = x1/10000;
x2 = x2/10000;
x3 = x3/10000;
x4 = x4/10000;

plot(x1, 'r', 'linewidth', 2);
hold on;
plot(x2, 'b', 'linewidth', 2);
plot(x3, 'g', 'linewidth', 2);
plot(x4, 'y', 'linewidth', 2);


tt = sprintf("Convergence Graph for %s", str);
title(tt);
ylabel('f(x)');
xlabel('Iterations');
legend(["Gradient Descent" "Conjugate Gradient Descent", "Newton's Method", "Quasi Newton Method"], "location", "NorthEast");
print(str,'-dpng')
