function s = backtrack_modified(func, g, s, v, x, r, al, A, b)

val = func(x);
lambda = v'*g*al;

while (min(b - A*(x+s*v) ) <= 0) || (func(x+s*v) > val + s*lambda)
    s = r*s;
end