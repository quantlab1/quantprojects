function a = backtrack(f, g, a, p, x, r, c)

val = f(x);
lambda = c * p'*g;

while f(x + a*p) > (val + a*lambda)
    a = r*a;
end