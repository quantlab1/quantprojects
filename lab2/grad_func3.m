function d = grad_func3(X)
% d = gradient of the function at X

t = X(2) - X(1)*X(1);

d = [-400 * t * X(1) - 2 * (1-X(1)) ; 200 * t];