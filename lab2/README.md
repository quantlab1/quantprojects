# README #

Unconstrained Optimization

### What is this project for? ###

* This project is optimizing the provided functions using various 
optimization algorithms like Gradient-Descent, Conjugate Gradient Descent,
Newton Method, Quasi-Newton Method

### How do I run the code ? ###

* cd /path/to/project/directory
* run main.m
The mimimum value for the functions and the point of mimium is available in
`val{1,2,3}_{GD,CGD,N,QN}` and `X{1,2,3}_{GD,CGD,N,QN}` variables. i.e.
val1_GD is the mimimum value obtained for function 1 with Gradient Descent
 method and X1_GD is the point of mimimum value.