function H = hessian_func3(X)
% H = Hessian of the function at X

H = [ -400*X(2) + 1200*X(1)*X(1) + 2 -400*X(1); -400*X(1) 200];
