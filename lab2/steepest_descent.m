function [X, f, st] = steepest_descent(func, grad_func, X, alpha, rho, c, threshold, backtrack_func)
% func - function to optimize
% grad_func - gradient of function func
% X - Initial value of Decision variable
% alpha - Starting value of alpha
% rho - provided by question
% c - some constant provided by question
% threshold - some threshold to stop the loop

M = 50000;

g = grad_func(X);
f = func(X);
st = f;

while (norm(g) > threshold) && ( M > 1)
    p = -g;
    
    alpha = backtrack_func(func, g, alpha, p, X, rho, c);
    
    X = X + alpha * p;
    f = func(X);
    g = grad_func(X);
    M = M - 1;
    st = [st f];
end