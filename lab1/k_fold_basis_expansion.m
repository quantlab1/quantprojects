function [p, test_error] = k_fold_basis_expansion(X, Y, K_MAX, P_MAX)
%X Input Vector
%Y Output Vector
%K_MAX Max no of folds to try
%P_MAX Max no of Polynomials to try

[L, ~] = size(X);

R = zeros(K_MAX -1, P_MAX);
R_t = zeros(K_MAX -1, P_MAX);

for i = 0: P_MAX

    for k = 2: K_MAX
        r2 = 0;
        rt_2 = 0;
        c = floor(L/k);
        for j = 1: k
            a = zeros(L, 1);
            
            a(1+(j-1)*c:j*c) = 1;
            a = logical(a);
            X_t = X(a, :);
            Y_t = Y(a);
            X_K = X(~a, :);
            Y_K = Y(~a);

            Z_K = ones(length(X_K), 1);
            Z_t = ones(length(X_t), 1);

            for l = 1: i
                Z_K = [Z_K X_K.^l];
                Z_t = [Z_t X_t.^l];
            end

            Z_K = Z_K';
            Z_t = Z_t';

            s = Z_K*Y_K;
            m = Z_K*Z_K';

            w = gaussian_elimination(m, s);
 
            e = Y_K - (w*Z_K)';
            e_t = Y_t - (w*Z_t)';

            r2 = r2 + dot(e, e);
            rt_2 = rt_2 + dot(e_t, e_t);
        end
        
        R(k-1, i+1) = r2/k;
        R_t(k-1, i+1) = rt_2/k;
    end
end

o = min(R_t(:));
[l, p] = find(R_t == o);
p = p-1;
test_error = o / (L/(l+1));

figure;
plot(R(l, 2:10), 'r', 'linewidth', 2);
hold on;
plot(R_t(l, 2:10), 'b', 'linewidth', 2);
title("Train error vs Test error");
ylabel('Error');
legend(["Train Error" "Test Error"], "location", "NorthEast");
print('TrainVsTestError','-dpng')
