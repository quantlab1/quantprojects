function [train_data, test_data] = normalize(train_data, test_data)

train_data = train_data/norm(train_data);
test_data = test_data/norm(train_data);

