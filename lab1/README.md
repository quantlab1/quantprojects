# README #

Linear Feature Engineering

### What is this project for? ###

* This project is for training a model give a training data and predict 
  the output of test inputs with the trained model.

### How do I run the code ? ###

* cd /path/to/project/directory
* run main.m
The predicted output of the test input data is saved to predictedOutput.txt
and the training error is available in `training_error` variable.