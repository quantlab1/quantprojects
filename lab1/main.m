clc;
clear;
traindata = importdata('traindata.txt');
testdata = importdata('testinputs.txt');

X = traindata(:,1:8);
Y = traindata(:,9);

% [X, Y] = normalize(X, Y);

X_t = testdata;

[L, ~] = size(X);

K_MAX = 10; % Max no of folds to try 
P_MAX = 10; % Max no of Polynomials to try

[p, test_error] = k_fold_basis_expansion(X, Y, K_MAX, P_MAX); % Polynomial order to fit.

Z = ones(length(X), 1);
Z_t = ones(length(X_t), 1);

for l = 1: p
    Z = [Z X.^l];
    Z_t = [Z_t X_t.^l];
end

Z = Z';
Z_t = Z_t';

s = Z*Y;
m = Z*Z';

w = gaussian_elimination(m, s);

e = Y - (w*Z)';

Y_t = (w*Z_t)';

r2 = dot(e, e);
training_error = r2/L;

fprintf("The training error for the provided training set is %.4f\n", training_error);
fprintf("The average test error using K-fold validation is %.4f\n", test_error);
dlmwrite('predictedOutput.txt',Y_t);
training_error
test_error
