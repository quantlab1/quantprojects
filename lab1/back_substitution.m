function x = back_substitution(A, b)

x = zeros(1,length(b));
[l, c] = size(A);

for i = c:-1:1
    su = 0;
    for j = i+1:l
        su = su +  (x(j) * A(i, j));
    end
    x(i) = ((b(i) - su)/A(i, i));
end