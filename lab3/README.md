### What is this project for? ###

* The purpose of the project is to check if the intervals provided by the 
given functions are valid and if they are valid find out the confidence 
level of the interval and see if the level is valid for all the datasizes 
or asymptotically.


### How do I run the code ? ###

* cd /path/to/project/directory
* run main.m
The results should display if the given functions are valid or not and if 
they are valid should provide the nearest confidence interval 
