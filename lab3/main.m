theta = 0.5; % true mean
alpha = 0.01; % (1-alpha) = confidence level
MAX_ITERATION = 10000;

for func_no = 1:10
    for N = [10, 100, 1000, 10000]
        h_ep = sqrt(log(2/alpha)/(2*N)); % Hoeffding's epsilon.
        
        h = 0; % No of hits
        ph = 0; % percentage of hits
        o = 0; % No of out of Range intervals;
        valid = 0; % Represents the validity of the function
       
        for k = 1:MAX_ITERATION
            X = sample_bernoulli(N, theta);
            X_mean = mean(X);
            [a, b] = ci(X, func_no);
            c = X_mean - h_ep;
            d = X_mean + h_ep;
            
            if(a >= (X_mean - h_ep)) && (b <= (X_mean + h_ep))
                o = o + 1;
            end
            
            if (a <= theta) && (theta <= b)
                h = h + 1;
            end
        end
        
        p = o/MAX_ITERATION; % Percentage of the interval being inside the range specified by Hoeffding?s inequality. 
        if (p >= .95)
            valid = 1;
        end
        
        if (valid)
            ph = h/MAX_ITERATION;
        end
        
        if(valid)
            fprintf('function: %2d N: %5d Is Valid ?:  %1d alpha:  %1.3f frac missed:  %1.3f\n', func_no, N, valid, alpha, 1-ph);
        else
            fprintf('function: %2d N: %5d Is Valid ?:  %1d\n', func_no, N, valid);
        end
    end
end